package utils;

public final class DbConfig {
	public static final String MYSQL_DB_URL = "jdbc:mysql://localhost:3306/";
	public static final String DB_NAME = "USER_DB";
	public static final String TABLE_NAME = "USERS";
}
