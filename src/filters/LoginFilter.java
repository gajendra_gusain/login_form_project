package filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginFilter
 */
public class LoginFilter implements Filter {
	private static final long serialVersionUID = 1L;
	private Logger logger;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginFilter() {
		super();
		logger = Logger.getLogger("LoginFilter");
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(false);
		String loginURI = req.getContextPath() + "/pages/jsp/login.jsp";
		if (session != null && session.getAttribute("name") != null) {
			request.getRequestDispatcher("helloPage").forward(request, response);
			filterChain.doFilter(request, response);
		} else
			res.sendRedirect(loginURI);

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		logger.log(Level.INFO, "--------------- initialized --------------------");
	}

}
