package filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;

/**
 * Servlet Filter implementation class CrudOperationFilter
 */
public class CrudOperationFilter implements Filter {
	private Logger logger;

	/**
	 * Default constructor.
	 */
	public CrudOperationFilter() {
		logger = Logger.getLogger("CrudOperationFilter");
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		logger.log(Level.INFO, "--------------- invoked ------------------");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(false);
		String loginURI = req.getContextPath() + "/pages/jsp/login.jsp";
		JsonObject jo = new JsonObject();

		if (session == null) {
			logger.log(Level.INFO, "-------session is null-------");
			jo.addProperty("redirectUrl", loginURI);
			res.getWriter().print(jo);
		} else {
			logger.log(Level.INFO, "-------session not null-------");
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		logger.log(Level.INFO, "---------------- initialized------------------");
	}

}
