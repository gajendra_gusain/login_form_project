package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.google.gson.JsonObject;
import com.mysql.cj.jdbc.MysqlDataSource;

import utils.DbConfig;

/**
 * Servlet implementation class ProcessLogin
 */
public class ProcessLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger;	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProcessLogin() {
		super();
		logger = Logger.getLogger("ProcessLogin");
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.log(Level.INFO, "------------- initialized --------------");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.log(Level.INFO, "------------ request recieved -------------");
		HttpSession session = request.getSession();
		logger.log(Level.INFO, "--------sessionId: " + session.getId());
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		Object[] params = getDatasourceParams(name, password);
		Boolean success = (Boolean) params[0];
		JsonObject jo = new JsonObject();
		String error = (params[2] != null) ? (String) params[2] : null;
		if (success.booleanValue()) {
			session.setAttribute("datasource", params[1]);
			session.setAttribute("name", request.getParameter("name"));
			response.sendRedirect(request.getContextPath() + "/helloPage");
		} else {
			jo.addProperty("error", error);
			response.getWriter().print(jo);
		}

	}

	private Object[] getDatasourceParams(String name, String password) {
		Object[] params = new Object[3];
		Boolean success = false;
		MysqlDataSource mds = null;
		try {
			mds = new MysqlDataSource();
			mds.setURL(DbConfig.MYSQL_DB_URL);
			mds.setUser(name);
			mds.setPassword(password);
			Connection c = mds.getConnection();
			success = true;
			c.close();
			initializeTables(mds);
		} catch (Exception e) {
			e.printStackTrace();
			params[2] = e.getMessage();
		}
		params[0] = success;
		params[1] = mds;
		return params;
	}

	private static void initializeTables(DataSource ds) throws Exception {
		final String createDatabase = "CREATE DATABASE IF NOT EXISTS " + DbConfig.DB_NAME;
		final String createTable = "CREATE TABLE IF NOT EXISTS "+DbConfig.DB_NAME+"."+DbConfig.TABLE_NAME+" ( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, user VARCHAR(50) NOT NULL)";
		Connection c = null;
		try {
			c = ds.getConnection();
			Statement s = c.createStatement();
			s.executeUpdate(createDatabase);
			s.executeUpdate(createTable);
			s.close();
		} finally {
			if (c != null)
				c.close();
		}
	}

}
