package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import utils.DbConfig;

/**
 * Servlet implementation class CRUDOperationServlet
 */
public class CRUDOperationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CRUDOperationServlet() {
		super();
		logger = Logger.getLogger("CRUDOperationServlet");
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.log(Level.INFO, "-----------initialized--------------");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// to keep session alive
		HttpSession session = request.getSession(false);
		if (session != null) {
			String operation = request.getParameter("operation");
			JsonObject jo = performOperation(operation, request);
			response.setContentType("application/json");
			response.getWriter().print(jo.toString());
		}
	}

	private JsonObject performOperation(final String operation, HttpServletRequest r) {
		DataSource ds = (DataSource) (r.getSession(false).getAttribute("datasource"));
		Connection c = null;
		JsonObject jo = null;
		try {
			c = ds.getConnection();
			Statement s = c.createStatement();
			s.executeQuery("use " + DbConfig.DB_NAME);
			ResultSet rs = null;
			StringBuilder queryBuilder = new StringBuilder();
			switch (operation) {
			case "create": {
				queryBuilder.setLength(0);
				String user = r.getParameter("user");
				jo = new JsonObject();
				if (!user.isEmpty()) {
					queryBuilder.append("INSERT INTO ").append(DbConfig.TABLE_NAME).append("(user)").append("values('")
							.append(user).append("')");
					logger.log(Level.INFO, "query: " + queryBuilder.toString());
					int rowsAffected = s.executeUpdate(queryBuilder.toString());
					if (rowsAffected > 0)
						jo.addProperty("inserted", r.getParameter("user"));
					else
						jo.addProperty("inserted", "none");
				} else
					jo.addProperty("error", "value can't be empty");
			}
				break;
			case "delete": {
				queryBuilder.setLength(0);
				String user = r.getParameter("user");
				jo = new JsonObject();
				if (!user.isEmpty()) {
					queryBuilder.append("DELETE FROM ").append(DbConfig.TABLE_NAME).append(" WHERE user='").append(user)
							.append("'");
					int rowsAffected = s.executeUpdate(queryBuilder.toString());
					if (rowsAffected > 0)
						jo.addProperty("deleted", r.getParameter("user"));
					else
						jo.addProperty("deleted", "none");
				} else
					jo.addProperty("error", "value can't be empty");
			}
				break;
			case "update": {
				queryBuilder.setLength(0);
				String newVal = r.getParameter("new");
				jo = new JsonObject();
				if (!newVal.isEmpty()) {
					queryBuilder.append("UPDATE ").append(DbConfig.TABLE_NAME).append(" SET user='").append(newVal).append("'");
					queryBuilder.append(" WHERE user='").append(r.getParameter("old")).append("'");
					int rowsAffected = s.executeUpdate(queryBuilder.toString());
					if (rowsAffected > 0)
						jo.addProperty("updated", r.getParameter("old"));
					else
						jo.addProperty("updated", "none");
				} else {
					jo.addProperty("error", "updated value can't be empty");
				}
			}
				break;
			case "show":
				queryBuilder.setLength(0);
				queryBuilder.append("SELECT *FROM USERS");
				logger.log(Level.INFO, "query: " + queryBuilder);
				rs = s.executeQuery(queryBuilder.toString());
				JsonArray ja = new JsonArray();
				while (rs.next()) {
					JsonObject o = new JsonObject();
					o.addProperty("id", rs.getInt("id"));
					o.addProperty("user", rs.getString("user"));
					ja.add(o);
				}
				jo = new JsonObject();
				jo.add("users", ja);
				break;
			default:
				logger.log(Level.INFO,
						"-------------------- Unknown Operation: " + operation + "--------------------------s");

			}
			s.close();
			c.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return jo;
	}

}
