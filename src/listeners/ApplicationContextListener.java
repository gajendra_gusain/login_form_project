package listeners;

import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Application Lifecycle Listener implementation class JSPContextListener
 *
 */
public class ApplicationContextListener implements ServletContextListener {
	private Logger logger;
	private static ServletContext sc;

	/**
	 * Default constructor.
	 */
	public ApplicationContextListener() {
		// TODO Auto-generated constructor stub
		logger = Logger.getLogger("JSPContextListener");
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		sc = arg0.getServletContext();
	}

}
