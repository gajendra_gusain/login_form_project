package listeners;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class UserSessionListener
 *
 */
public class UserSessionListener implements HttpSessionListener {
	private Logger logger;

	/**
	 * Default constructor.
	 */
	public UserSessionListener() {
		logger = Logger.getLogger("UserSessionListener");
	}

	/**
	 * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent arg0) {
		logger.log(Level.INFO, "---------------- sessionCreated ---------------");
	}

	/**
	 * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent arg0) {
		logger.log(Level.INFO, "------------------- sessionDestroyed ----------------------");
	}

}
