<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
var domIndex=0;
document.addEventListener("DOMContentLoaded", function(event) {
	var crudSelect=document.getElementById("crud_options");
	var divContainer = document.getElementById("container");
	var button=document.getElementById("click_button");
	button.addEventListener("click", function (event) {				
		var val=this.value;
		switch(val) {
			case 'create':
				var user=document.getElementById("user");				
				add(user.value);
				removeDOMChilds(divContainer);
				break;
			case 'update':
				var selectToUpdate=document.getElementById("selectToUpdate");
				var inputField=document.getElementById("updateField");
				update(selectToUpdate.value, inputField.value);
				removeDOMChilds(divContainer);
				break;
			case 'delete':
				var userSelect=document.getElementById("userSelect");
				_delete(userSelect.value);
				removeDOMChilds(divContainer);
				break;
			case 'show':
				createTable(divContainer);
				break;
		}
		crudSelect.selectedIndex=0;		
	}, false);
		
	crudSelect.addEventListener("change", function (event) {
		var val=this.value;				
		removeDOMChilds(divContainer);
		var button=document.getElementById("click_button");
		switch(val) {
			case 'create':				
				var container=document.createElement("div");
				var br=document.createElement("br");
				var user=document.createElement("input");									
				user.setAttribute("style","margin:5px");
				user.setAttribute("type", "text");
				user.setAttribute("id","user");
				user.setAttribute("name","user");				
				var userLabel=document.createElement("label");
				userLabel.setAttribute("for","user");
				userLabel.innerHTML ="Enter User:";				
				container.appendChild(userLabel);
				container.appendChild(user);				
				divContainer.appendChild(container, ++domIndex);				
				button.value="create";
				break;
			case 'update':				
				var selectToUpdate=document.createElement("select");
				var inputField=document.createElement("input");
				inputField.setAttribute("type","text");
				inputField.setAttribute("placeholder","New Value");
				inputField.setAttribute("style","margin:5px");
				inputField.setAttribute("id","updateField");
				selectToUpdate.setAttribute("id","selectToUpdate");
				var option=document.createElement("option");
				option.text="choose a user";
				option.value="";
				option.disabled=true;
				option.selected=true;
				selectToUpdate.add(option);
				var obj=JSON.parse(getUsers());
				for(var i=0; i< obj.users.length; i++) {
					var option = document.createElement("option");
					option.text = obj.users[i].user;
					option.value = obj.users[i].user;
					selectToUpdate.add(option);
				}
				divContainer.appendChild(selectToUpdate);
				divContainer.appendChild(inputField);				
				button.value="update";				
				break;
			case 'delete':
				var userSelect=document.createElement("select");
				userSelect.setAttribute("id","userSelect");
				var option=document.createElement("option");
				option.text="choose a user";
				option.value="";
				option.disabled=true;
				option.selected=true;
				userSelect.add(option);
				var obj=JSON.parse(getUsers());
				for(var i=0; i< obj.users.length; i++) {
					var option = document.createElement("option");
					option.text = obj.users[i].user;
					option.value = obj.users[i].user;
					userSelect.add(option);
				}
				divContainer.appendChild(userSelect);				
				button.value="delete";
				break;
			case 'show':				
				button.value="show";				
				break;
				default:
					alert("unknown option: "+val);
		}
	});	
    console.log("DOM fully loaded and parsed");
  });
    
    function add(u) {
    	var res;
		var xhttp=new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {		    			    
		    	alert(this.response);	
		    	redirectIfRequired(this.response);
		    }
		  };
		xhttp.open("POST", "/login_form_project/crudServlet", false);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("operation=create&user="+u);		
    }
    
    function update(old, _new) {
    	var res;
    	var xhttp=new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {		    			    
		    	alert(this.response);
		    	redirectIfRequired(this.response);
		    }
		  };
		xhttp.open("POST", "/login_form_project/crudServlet", false);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");		
		xhttp.send("operation=update&new="+_new+"&old="+old);
    }
    function _delete(u) {
    	var res;
    	var xhttp=new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {		    			    
		    	alert(this.response);
		    	redirectIfRequired(this.response);
		    }
		  };
		xhttp.open("POST", "/login_form_project/crudServlet", false);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("operation=delete&user="+u);
    }
    function removeDOMChilds(o) {
    	while (o.firstChild) {
    	    o.removeChild(o.firstChild);
    	}
    }
	function createTable(container) {
		var obj=JSON.parse(getUsers());		
		var col = [];
	    for (var i = 0; i < obj.users.length; i++) {
	        for (var key in obj.users[i]) {
	            if (col.indexOf(key) === -1) {
	                col.push(key);
	            }
	        }
	    }
	    
	 // CREATE DYNAMIC TABLE.
	    var table = document.createElement("table");
	 table.setAttribute("style","border: 1px solid gray; margin:5px");	 
	 
	 // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
	
	    var tr = table.insertRow(-1);                   // TABLE ROW.
	
	    for (var i = 0; i < col.length; i++) {
	        var th = document.createElement("th");      // TABLE HEADER.
	        th.setAttribute("style","border: 1px solid gray");
	        th.innerHTML = col[i];
	        tr.appendChild(th);
	    }
	    
	    // ADD JSON DATA TO THE TABLE AS ROWS.
	    for (var i = 0; i < obj.users.length; i++) {
	        tr = table.insertRow(-1);
	        for (var j = 0; j < col.length; j++) {
	            var tabCell = tr.insertCell(-1);
	            tabCell.innerHTML = obj.users[i][col[j]];
	            tabCell.setAttribute("style","border: 1px solid gray");
	        }
	    }
	 // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.       
        container.innerHTML = "";
        container.appendChild(table, ++domIndex);
	}
	
	function getUsers() {		
		var res;
		var xhttp=new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {		    			    
		    	res=this.response;
		    	redirectIfRequired(res);
		    }
		  };
		xhttp.open("POST", "/login_form_project/crudServlet", false);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");	
		xhttp.send("operation=show");		
		return res;
	}
	function redirectIfRequired(response) {
		var json=JSON.parse(response);
		if(json.hasOwnProperty("redirectUrl"))
    		window.location = json.redirectUrl;
	}
</script>
</head>
<body>
	<a href="/login_form_project/processLogout">logout</a>
	<h1>
		<%						
			out.print("Hello "+request.getSession(false).getAttribute("name"));
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setDateHeader("Expires", 0);
		%>
	</h1>
	<br>
	<select id="crud_options">
		<option value="" disabled selected>select operation</option>
		<option value="create">Add User</option>
		<option value="update">Update User</option>
		<option value="delete">Delete User</option>
		<option value="show">Show Users</option>
	</select>
	<br>
	<div id="container"></div>
	<button id="click_button" style="margin: 5px">submit</button>
</body>
</html>