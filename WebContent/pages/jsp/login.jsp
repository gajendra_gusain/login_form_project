<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
	.form {
		text-align: center;
		margin: auto;
		width: 50%;		
		padding: 30px;
	}
	#submit {
		margin:10px;
	}
	#name {
		margin: 5px;
	}
	#password {
		margin: 5px;
	}
</style>
</head>
<body>
<div class="form">
	<h1>Login</h1>
	<form action="/login_form_project/processLogin" method="post">
		<label for="name">Username</label>
		<input id="name" type="text" name="name" placeholder="MySQL username"/>
		<br>
		<label for="password">Password</label>		
		<input id="password" type="password" name="password" placeholder="MySQL password"/>
		<br>
		<input id="submit" type="submit" value="login"/>
	</form>	
</div>	
</body>
</html>